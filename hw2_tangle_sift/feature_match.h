#ifndef FEATURE_MATCH_H_
#define FEATURE_MATCH_H_

#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/legacy/legacy.hpp>
#include <opencv2/nonfree/nonfree.hpp> 
#include <opencv2/nonfree/features2d.hpp>

using namespace cv;

class FeatureMatch{
	
public:
	FeatureMatch();
	~FeatureMatch();
	bool SitfFeatureMatch();

private:
	void DetectSitfFeature();
	void ComputeSitfFeatureVector();
	void MatchSitfFeature();
	void ShowImage();
	Scalar GetScalar();

private:
	Mat image_source_, image_match_;
	Mat image_keypoint_source_, image_keypoint_match_;
	Mat image_match_result_;

	Mat feature_vector_source_, feature_vector_match_;

	std::vector<KeyPoint> keypoint_source_, keypoint_match_;
	std::vector<KeyPoint> new_keypoint_source_, new_keypoint_match_;

};
	
#endif//FEATURE_MATCH_H_