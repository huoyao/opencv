#include "feature_match.h"
#include <algorithm>
#include <sstream>
#include <string>

bool cmp(DMatch m1, DMatch m2)
{
	return m1 < m2;
};

FeatureMatch::FeatureMatch()
{

}

FeatureMatch::~FeatureMatch()
{

}

// use sitf to complete feature match
bool FeatureMatch::SitfFeatureMatch()
{
  image_source_ =imread("zju-lib1.jpg");
	image_match_ = imread("zju-lib2.jpg");
	if (!image_source_.data || !image_match_.data)
	{
		return false;
	}

	DetectSitfFeature();
	ComputeSitfFeatureVector();
	MatchSitfFeature();

	return true;
}

//detect sitf feature
void FeatureMatch::DetectSitfFeature()
{
	SiftFeatureDetector detector;

	detector.detect(image_source_, keypoint_source_);
	detector.detect(image_match_, keypoint_match_);
}

//compute sitf feature vector
void FeatureMatch::ComputeSitfFeatureVector()
{
	SiftDescriptorExtractor extractor;

	extractor.compute(image_source_, keypoint_source_, feature_vector_source_);
	extractor.compute(image_match_, keypoint_match_, feature_vector_match_);
}

//match feature between image_source_ and image_match_
void FeatureMatch::MatchSitfFeature()
{
	BruteForceMatcher<L2<float>> matcher;
	std::vector<DMatch> matches;
	
	matcher.match(feature_vector_source_, feature_vector_match_, matches);
	sort(matches.begin(), matches.begin() + matches.size(), cmp);
	matches.erase(matches.begin() + 200, matches.end());

	//set new keypoints
	for (size_t i = 0; i < matches.size(); i++)
	{
		new_keypoint_source_.push_back(keypoint_source_[matches[i].queryIdx]);
		new_keypoint_match_.push_back(keypoint_match_[matches[i].trainIdx]);
	}

	//draw image and it's key points
	for (size_t i = 0; i < new_keypoint_source_.size(); i++)
	{
		Scalar temp_scalar = GetScalar();
		circle(image_source_, new_keypoint_source_[i].pt, new_keypoint_source_[i].size * 4, temp_scalar, 2);
		circle(image_match_, new_keypoint_match_[i].pt, new_keypoint_match_[i].size * 4, temp_scalar, 2);
	}

	string str = "image's match-feature's number is ";
	std::stringstream ss;
	ss << matches.size();
	str += ss.str();
 // resize(image_source_,image_source_,Size(0,0),0.4,0.4);
  //resize(image_match_,image_match_,Size(0,0),0.4,0.4);
	//CvFont font;  
	//cvInitFont(&font,CV_FONT_HERSHEY_SIMPLEX | CV_FONT_ITALIC, 1,1,0,4);
	putText(image_source_, str.c_str(),cvPoint(image_match_.rows * 0.6,image_match_.cols*0.6), FONT_HERSHEY_PLAIN,2, CV_RGB(255, 0, 0));
	putText(image_match_, str.c_str(),cvPoint(image_match_.rows * 0.6,image_match_.cols*0.6),FONT_HERSHEY_PLAIN,2, CV_RGB(255, 0, 0));

  imshow("image source feature", image_source_);
  imshow("image match feature", image_match_);

	imwrite("zju-lib1_match_result.jpg", image_source_);
	imwrite("zju-lib2_match_result.jpg", image_match_);

	waitKey(0);
}

//get every point's CVScalar
Scalar FeatureMatch::GetScalar()
{
	return Scalar(rand() % 256, rand() % 256, rand() % 256);
}

