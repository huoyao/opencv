#include <iostream>
#include <string>
#include <strstream>
#include <opencv2/core/core.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/nonfree/nonfree.hpp>
using namespace std;
using namespace cv;

int main1(int argc,char **argv){
  namedWindow("show_image1");
  namedWindow("show_image2");
  initModule_nonfree();
  Mat image1=imread("zju-lib1.jpg");
  Mat image2=imread("zju-lib2.jpg");
  //detect keypoints
  Ptr<FeatureDetector> detector=FeatureDetector::create("SIFT");
  vector<KeyPoint> keypoints_1,keypoints_2;
  keypoints_1.clear();
  keypoints_2.clear();
  detector->detect(image1,keypoints_1);
  detector->detect(image2,keypoints_2);
  //descriptors
  Ptr<DescriptorExtractor> descriptor=DescriptorExtractor::create("SIFT");
  Mat descriptor_1,descriptor_2;
  descriptor->compute(image1,keypoints_1,descriptor_1);
  descriptor->compute(image2,keypoints_2,descriptor_2);
  vector<DMatch> dmatch;
  Ptr<DescriptorMatcher> matcher=DescriptorMatcher::create("BruteForce");
  matcher->match(descriptor_1,descriptor_2,dmatch);
  double max_dist = dmatch[0].distance;
  double min_dist = dmatch[0].distance;
  //Quick calculation of max and min distances between keypoints
  for(vector<DMatch>::const_iterator itr=dmatch.cbegin(); itr!=dmatch.cend(); ++itr)
  { 
    double dist = itr->distance;
    if( dist < min_dist ) min_dist = dist;
    if( dist > max_dist ) max_dist = dist;
  }
  //-- Draw only "good" matches (i.e. whose distance is less than 2*min_dist )
  //-- PS.- radiusMatch can also be used here.
  std::vector< DMatch > good_matches;
  for(vector<DMatch>::const_iterator itr=dmatch.cbegin(); itr!=dmatch.cend(); ++itr)
  { 
    if(itr->distance <= 2*min_dist )
    {
      good_matches.push_back(*itr); 
    }
  }
  cout<<dmatch.size()<<endl;
  cout<<good_matches.size();
  //select good keypoints
  vector<KeyPoint> keypoints_1_,keypoints_2_;
  keypoints_1_.clear();
  keypoints_2_.clear();
  for (int i=0;i!=good_matches.size();++i)
  {
    if (keypoints_1[good_matches[i].queryIdx].size<=20)
    {
      keypoints_1_.push_back(keypoints_1[good_matches[i].queryIdx]);
      keypoints_2_.push_back(keypoints_2[good_matches[i].trainIdx]);
    }
  }
   cout<<keypoints_1_.size()<<endl;
   cout<<keypoints_2_.size();
  //put text onto image with string handling
  string str;
  stringstream ss;
  ss<<keypoints_1_.size();
  ss>>str;
  str="matched points:"+str;

  //draw circles onto image with scale
  int r,g,b;
  for (int iter=0;iter!= keypoints_1_.size();++iter)  
  {  
    b=rand()%256;
    g=rand()%256;
    r=rand()%256;
    Point center= keypoints_1_[iter].pt;  
    circle(image1,center,keypoints_1_[iter].size,Scalar(r,g,b),5);
    center= keypoints_2_[iter].pt;
    circle(image2,center,keypoints_2_[iter].size,Scalar(r,g,b),5);
  } 
  //To adapt picture to the screen
  resize(image1,image1,Size(0,0),0.4,0.4);
  resize(image2,image2,Size(0,0),0.4,0.4);
  putText(image1,str, cvPoint(10,image1.cols*0.75), FONT_HERSHEY_PLAIN, 1 ,Scalar(255,255,0));
  imwrite("3.jpg",image1);
  imwrite("4.jpg",image2);
  imshow("show_image1",image1);
  imshow("show_image2",image2);
  cvWaitKey(0);
  cvDestroyAllWindows();
  return 0;
}
