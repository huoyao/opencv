#include "highgui.h"
#include "cv.h"
#include <iostream>
#include <Eigen/Core>
#include <Eigen/Dense>

class ColorTransfer
{
public:
  ColorTransfer(void);
  ~ColorTransfer(void);
  bool LoadImage( const char* imagename_s, const char* imagename_t );

  void InitMatrix(Eigen::Matrix3f rgbtolms,
                  Eigen::Matrix3f lmstolab,
                  Eigen::Matrix3f labtolms,
                  Eigen::Matrix3f lmstorgb);
  void InitImageVector( const IplImage* src, Vector3<double>* dst );
  void TransferRGB2LAB( Vector3<double>* image_V, const int& height, const int& width );
  void TransferLAB2RGB( Vector3<double>* image_V, const int& height, const int& width );
  void OutputVector( const Vector3<double>& v );
  void GetMeanVector( Vector3<double>& mean, const Vector3<double>* image_V, const int& height, const int& width );
  void GetDeltaVector( Vector3<double>& delta, const Vector3<double>& mean, const Vector3<double>* image_V, const int& height, const int& width );
  void GetFinalLAB();
  int AdjustNum( double a );
  void ApplyChangeOnImage( IplImage* dst, const Vector3<double>* image_V );
  void OutputImageV( const Vector3<double>* image_sV, const int& height, const int& width );
  void InitData();
  void RunCT();
private:
  Vector3<double>* image_sV;
  Vector3<double>* image_tV;	
  Vector3<double> mean_s, mean_t, delta_s, delta_t;
  int height_s, width_s, height_t, width_t;
  IplImage* image_s;
  IplImage* image_t;
  IplImage* image_r;
  Eigen::Matrix3f rgbtolms;
  Eigen::Matrix3f lmstolab;
  Eigen::Matrix3f labtolms;
  Eigen::Matrix3f lmstorgb;
};
