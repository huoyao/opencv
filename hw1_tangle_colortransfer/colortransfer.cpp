#include "ReinHardCT.h"
#include <cmath>

#include <string.h>

Matrix<double> RGB2LMS_M( 0.3811, 0.5783, 0.0402,
              0.1967, 0.7244, 0.0782,
              0.0241, 0.1288, 0.8444 );
Matrix3<double> LMS2LAB_M = Matrix3<double>( 1.0/sqrt( 3.0 ), 0, 0,
                      0, 1.0/sqrt( 6.0 ), 0,
                      0, 0, 1.0/sqrt( 2.0 ) ) 
                      *
                      Matrix3<double>( 1, 1, 1,
                      1, 1, -2,
                      1, -1, 0 );		
Matrix3<double> LMS2RGB_M( 4.4679, -3.5873, 0.1193,
              -1.2186, 2.3809, -0.1624,
              0.0497, -0.2439, 1.2045 );
Matrix3<double> LAB2LMS_M = Matrix3<double>( 1, 1, 1,
                      1, 1, -1,
                      1, -2, 0 )		  
                      *
                      Matrix3<double>( sqrt( 3.0 )/3.0, 0, 0,
                      0, sqrt( 6.0 )/6.0, 0,
                      0, 0, sqrt( 2.0 )/2.0 );


ReinHardCT::ReinHardCT(void)
{
}

ReinHardCT::~ReinHardCT(void)
{
}
void ReinHardCT::InitImageVector( const IplImage* src, Vector3<double>* dst ) 
{
  for ( int row = 0; row < src->height; ++row )
  {
    for ( int col = 0; col < src->width; ++col )
    {
      int index = row * src->width + col;
      dst[index][0] = (unsigned char)src->imageData[row * src->widthStep + col * 3 + 2];
      dst[index][1] = (unsigned char)src->imageData[row * src->widthStep + col * 3 + 1];
      dst[index][2] = (unsigned char)src->imageData[row * src->widthStep + col * 3];
      //OutputVector( dst[index] );
      //getchar();
    }
  }
}

void ReinHardCT::TransferRGB2LAB( Vector3<double>* image_V, const int& height, const int& width ) 
{
  for ( int row = 0; row < height; ++row )
  {
    for ( int col = 0; col < width; ++col )
    {
      int index = row * width + col; 
      image_V[index] = RGB2LMS_M * image_V[index];//equation 1
      for ( int i = 0; i < 3; ++i )
      {//equation 2
        if ( image_V[index][i] == 0 )
        {
          image_V[index][i] += 1;
          printf("zero pixel index = %d, i = %d\n", index , i);
        }
        image_V[index][i] = log10( image_V[index][i] );
      }
      image_V[index] = LMS2LAB_M * image_V[index];//equation 3

    }
  }
}
void ReinHardCT::TransferLAB2RGB( Vector3<double>* image_V, const int& height, const int& width ) 
{
  for ( int row = 0; row < height; ++row )
  {
    for ( int col = 0; col < width; ++col )
    {
      int index = row * width + col; 
      image_V[index] = LAB2LMS_M * image_V[index];//the invert of equation 1
      for ( int i = 0; i < 3; ++i )
      {//the invert of equation 2 
        image_V[index][i] = pow( (double)10, image_V[index][i] );
      }
      image_V[index] = LMS2RGB_M * image_V[index];// the invert of equation 3
    }
  }
}

void ReinHardCT::OutputVector( const Vector3<double>& v ) 
{
  for ( int i = 0; i <3 ;++i )
  {
    if ( i ==2 )
    {
      printf("%.2f ",v[i]);
    }
    else printf("%.2f,",v[i]);
  }
}

void ReinHardCT::GetMeanVector( Vector3<double>& mean, const Vector3<double>* image_V, const int& height, const int& width ) 
{
  mean[0] = 0.0; mean[1] = 0.0; mean[2] = 0.0;
  for ( int row = 0; row < height; ++row )
  {
    for ( int col = 0; col < width; ++col )
    {
      int index = row * width + col; 
      mean += image_V[index];
    }

  }
  //mean /= (double)( width * height );
  //mean = mean / (double)( width * height );
  double divide = (double)( width * height );
  mean[0] /= divide; mean[1] /= divide; mean[2] /= divide;
  OutputVector( mean );
  printf("\n");
}

void ReinHardCT::GetDeltaVector( Vector3<double>& delta, const Vector3<double>& mean, const Vector3<double>* image_V, const int& height, const int& width ) 
{
  Vector3<double> square_sum;
  square_sum[0] = 0.0; square_sum[1] = 0.0; square_sum[2] = 0.0;
  delta[0] = 0.0; delta[1] = 0.0; delta[2] = 0.0;
  for ( int row = 0; row < height; ++row )
  {
    for ( int col = 0; col < width; ++col )
    {
      int index = row * width + col;
      Vector3<double> temp = image_V[index];
      temp -= mean;
      temp[0] *= temp[0]; temp[1] *= temp[1]; temp[2] *= temp[2]; 
      square_sum += temp;
    }
  }
  int N = width * height; 
  square_sum[0] /= (double)N; square_sum[1] /= (double)N; square_sum[2] /= (double)N;
  delta[0] = sqrt( square_sum[0] );
  delta[1] = sqrt( square_sum[1] );
  delta[2] = sqrt( square_sum[2] );
  OutputVector( delta );
  printf("\n");
}

void ReinHardCT::GetFinalLAB() 
{
  Vector3<double> delta_d;
  for ( int i = 0; i < 3; ++i )
  {
    delta_d[i] = delta_t[i] / delta_s[i]; 
  }
  printf("delta_d....\n");
  OutputVector( delta_d );
  printf("\n");
  for ( int row = 0; row < height_s; ++row )
  {
    for ( int col = 0; col < width_s; ++col )
    {
      int index = row * width_s + col;
      image_sV[index] -= mean_s;
      for ( int i = 0; i < 3; ++i )
      {
        image_sV[index][i] = delta_d[i] * image_sV[index][i]; 
      }
      image_sV[index] += mean_t;
      //OutputVector( image_sV[index] );
    }
  }
}

int ReinHardCT::AdjustNum( double a )
{
  int res = (int)a;
  res = __min( 255, __max( 0, res ) );
  return res;
}
void ReinHardCT::ApplyChangeOnImage( IplImage* dst, const Vector3<double>* image_V ) 
{
  for ( int row = 0; row < dst->height; ++row )
  {
    for ( int col = 0; col < dst->width; ++col )
    {
      int index = row * dst->width + col;
      for ( int i = 0; i < 3; ++i )
      {
        int a = AdjustNum( image_V[index][2 - i] );
        //printf("%d\n",a);
        dst->imageData[row * dst->widthStep + col * 3 + i] = a;
        //dst->imageData[row * dst->widthStep + col * 3 + i] = (int)10;
      }
    }
  }
}

void ReinHardCT::OutputImageV( const Vector3<double>* image_sV, const int& height, const int& width ) 
{
  for ( int row = 0; row < height; ++row )
  {
    for ( int col = 0; col < width; ++col )
    {
      OutputVector( image_sV[row * width + col] );
      printf(" ");
    }
    printf("\n");
  }
}

void ReinHardCT::InitData()
{
  image_sV = new Vector3<double>[image_s->imageSize];
  image_tV = new Vector3<double>[image_t->imageSize];
  height_s = image_s->height;
  width_s = image_s->width;
  height_t = image_t->height;
  width_t = image_t->width;
  //transfer the image to vector mode
  InitImageVector( image_s, image_sV);
  printf("initial image_sV....\n");
  OutputImageV( image_sV, height_s, width_s );
  InitImageVector( image_t, image_tV);
}

void ReinHardCT::Solve()
{
  image_r =  cvCreateImage( cvGetSize(image_s), IPL_DEPTH_8U, 3 );
  image_r = cvCloneImage( image_s );
  /*cvShowImage( "image_s", image_s );
  cvShowImage( "image_t", image_t );
  cvWaitKey(0);*/
  InitData();
  /*ApplyChangeOnImage( image_r, image_sV );
  cvShowImage( "image_s", image_s );
  cvShowImage( "image_r", image_r );
  cvWaitKey(0); */
  //transfer the rgb space to LAB space equation 1,2,3
  printf("TransferRGB2LAB....\n");
  TransferRGB2LAB( image_sV, height_s, width_s );
  printf("after TransferRGB2LAB image_sV now....\n");
  OutputImageV( image_sV, height_s, width_s );
  TransferRGB2LAB( image_tV, height_t, width_t );
  //get the mean and delta
  printf("solve mean_s....\n");
  GetMeanVector( mean_s, image_sV, height_s, width_s );
  printf("solve mean_t....\n");
  GetMeanVector( mean_t, image_tV, height_t, width_t );
  printf("solve delta_s....\n");
  GetDeltaVector( delta_s, mean_s, image_sV, height_s, width_s );
  printf("solve delta_t....\n");
  GetDeltaVector( delta_t, mean_t, image_tV, height_t, width_t );
  //get the final LAB equation 4,5
  GetFinalLAB();
  //transfer the LAB space back to RGB space
  TransferLAB2RGB( image_sV, height_s, width_s );
  printf("after TransferLAB2RGB image_sV....\n");
  OutputImageV( image_sV, height_s, width_s );
  //apply the transfer on the image and show them
  ApplyChangeOnImage( image_r, image_sV );
  cvShowImage( "image_s", image_s );
  cvShowImage( "image_t", image_t );
  cvShowImage( "ReinHardCT", image_r );
  cvSaveImage( "output_1.png", image_r );
}

bool ReinHardCT::LoadImage( const char* imagename_s, const char* imagename_t )
{
  image_s = cvLoadImage( imagename_s );
  image_t = cvLoadImage( imagename_t );
  if ( !image_s || ! image_t )
  {
    printf("Load image error!\n");
    return false;
  }
  return true;
}
