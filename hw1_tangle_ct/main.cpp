#include <iostream>
#include "ColorTransfer.h"
using namespace std;

int main(int argc,char **argv){
  cv::namedWindow("souce image",CV_WINDOW_AUTOSIZE);
  cv::namedWindow("target image",CV_WINDOW_AUTOSIZE);
  cv::namedWindow("result image",CV_WINDOW_AUTOSIZE);
  tl::ColorTransfer CT;
  CT.RunColorTransfer();
  cv::imshow("souce image",CT.GetImgeSouce());
  cv::imshow("target image",CT.GetImgeTarget());
  cv::imshow("result image",CT.GetImgeResult());
  cvWaitKey(0);
  cvDestroyAllWindows();
  return 0;
}
