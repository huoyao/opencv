#include "colortransfer.h"
#include <iostream>
#include <vector>
using namespace tl;

ColorTransfer::ColorTransfer(void){

}

ColorTransfer::~ColorTransfer(void){

}

bool ColorTransfer::LoadImage( const std::string imagename_souce, const std::string imagename_target)
{
  image_souce_=cv::imread(imagename_souce);
  image_target_=cv::imread(imagename_target);
  image_result_=cv::imread(imagename_souce);
  if (image_souce_.empty() || image_target_.empty())
  {
    return false;
  } 
  else
  {
    return true;
  }
}

void ColorTransfer::InitMatrix(){
  rgbtolms_<<0.3811, 0.5783, 0.0402,
             0.1967, 0.7244, 0.0782,
             0.0241, 0.1288, 0.8444;
  Eigen::Matrix3d temp1;
  temp1<< 1.0/sqrt( 3.0 ), 0, 0,
          0, 1.0/sqrt( 6.0 ), 0,
          0, 0, 1.0/sqrt( 2.0 );
  Eigen::Matrix3d temp2;
  temp2<<1, 1, 1,
         1, 1, -2,
         1, -1, 0;
  //std::cout<<temp1;
  lmstolab_=temp1*temp2;
  lmstorgb_<<4.4679, -3.5873, 0.1193,
             -1.2186, 2.3809, -0.1624,
             0.0497, -0.2439, 1.2045;
  temp1<<1, 1, 1,
         1, 1, -1,
         1, -2, 0;
  temp2<<sqrt( 3.0 )/3.0, 0, 0,
         0, sqrt( 6.0 )/6.0, 0,
         0, 0, sqrt( 2.0 )/2.0;
  labtolms_=temp1*temp2;
}

void ColorTransfer::GetPixelData( const cv::Mat &img, Eigen::MatrixXd &pixel_data){
  for (int i=0;i!=img.rows;++i)
  {
    for (int j=0;j!=img.cols;++j)
    {
      int idx=i*img.cols+j;
      //std::vector<double> temp;
      for (int k=0;k!=3;++k)
      {
        if (img.channels()==3)
        {
           pixel_data(idx,k)=(unsigned char)img.at<cv::Vec3b>(i,j)[2-k];
          //std::cout<<img.at<cv::Vec3b>(i,j)[2-k];
          //temp.push_back(img.at<cv::Vec3b>(i,j)[2-k]);
        }
      }
      //pixel_data[idx]=temp;
    }
  }
}

void ColorTransfer::InitTransfer(){
  height_souce_=image_souce_.cols;
  width_souce_=image_souce_.rows;
  height_target_=image_target_.cols;
  width_target_=image_target_.rows;
  image_souce_v_.resize(height_souce_*width_souce_,3);
  image_target_v_.resize(height_target_*width_target_,3);
  GetPixelData(image_souce_,image_souce_v_);
  GetPixelData(image_target_,image_target_v_);
}


void ColorTransfer::RgbtoLms( Eigen::MatrixXd &img_v, const int& height, const int& width){
  for (int i=0;i!=height;++i)
  {
    for (int j=0;j!=width;++j)
    {
      int idx=i*width+j;
      Eigen::MatrixXd temp(3,1);
      temp(0,0)=img_v(idx,0);
      temp(1,0)=img_v(idx,1);
      temp(2,0)=img_v(idx,2);
      temp=rgbtolms_*temp;
      for (int k=0;k!=3;++k)
      {
        if(temp(k,0)!=0)
          temp(k,0)=log10(temp(k,0));
      }
      temp=lmstolab_*temp;
      img_v(idx,0)=temp(0,0);
      img_v(idx,1)=temp(1,0);
      img_v(idx,2)=temp(2,0);
    }
  }
}

void ColorTransfer::LabtoRgb(Eigen::MatrixXd &img_v, const int& height, const int& width){
  for (int i=0;i!=height;++i)
  {
    for (int j=0;j!=width;++j)
    {
      int idx=i*width+j;
      Eigen::MatrixXd temp(3,1);
      temp(0,0)=img_v(idx,0);
      temp(1,0)=img_v(idx,1);
      temp(2,0)=img_v(idx,2);
      temp=labtolms_*temp;
      for (int k=0;k!=3;++k)
      {
          temp(k,0)=pow(10.0,temp(k,0));
      }
      temp=lmstorgb_*temp;
      img_v(idx,0)=temp(0,0);
      img_v(idx,1)=temp(1,0);
      img_v(idx,2)=temp(2,0);
    }
  }
}

void ColorTransfer::GetMean(Eigen::Vector3d &mean, const Eigen::MatrixXd &img_v, const int& height, const int& width){
  mean[0]=0.0;mean[1]=0.0;mean[2]=0.0;
  for (int i=0;i!=height;++i)
  {
    for (int j=0;j!=width;++j)
    {
      int idx=i*width+j;
      mean(0)=mean(0)+img_v(idx,0);
      mean(1)=mean(1)+img_v(idx,1);
      mean(2)=mean(2)+img_v(idx,2);
    }
  }
  double area=static_cast<double>(height*width);
  mean/=area;
}

void ColorTransfer::GetDelta(Eigen::Vector3d &delta, const Eigen::Vector3d &mean, const Eigen::MatrixXd &img_v, const int& height, const int& width){
  delta[0]=0.0;delta[1]=0.0;delta[2]=0.0;
  for (int i=0;i!=height;++i)
  {
    for (int j=0;j!=width;++j)
    {
      int idx=i*width+j;
      for (int k=0;k!=3;++k)
      {
        delta[k]+=(img_v(idx,k)-mean[k])*(img_v(idx,k)-mean[k]);
      }
    }
  }
  double area=static_cast<double>(height*width);
  delta/=area;
  delta(0)=sqrt(delta(0));
  delta(1)=sqrt(delta(1));
  delta(2)=sqrt(delta(2));
}

void ColorTransfer::Transfer(const int& height, const int& width){
  Eigen::Vector3d delta;
  delta[0]=delta_target_[0]/delta_souce_[0];
  delta[1]=delta_target_[1]/delta_souce_[1];
  delta[2]=delta_target_[2]/delta_souce_[2];
  for (int i=0;i!=height;++i)
  {
    for (int j=0;j!=width;++j)
    {
      int idx=i*width+j;
      for (int k=0;k!=3;++k)
      {
        image_souce_v_(idx,k)-=mean_souce_(k);
        image_souce_v_(idx,k)*=delta(k);
        image_souce_v_(idx,k)+=mean_target_(k);
        //cv::calcHist(image_souce_,chan);
        //cv::split(m,m,m,m,m);
      }
    }
  }
}

void ColorTransfer::GetResultImage(cv::Mat &img_rerult, const Eigen::MatrixXd &image_V ){
  for (int i=0;i!=image_souce_.rows;++i)
  {
    for (int j=0;j!=image_souce_.cols;++j)
    {
      int idx=i*image_souce_.cols+j;
      for (int k=0;k!=3;++k)
      {
        if (image_V(idx,k)<0)
        {
          img_rerult.at<cv::Vec3b>(i,j)[2-k]=0;
          continue;
        }else if (image_V(idx,k)>255)
        {
          img_rerult.at<cv::Vec3b>(i,j)[2-k]=255;
          continue;
        }else{
          img_rerult.at<cv::Vec3b>(i,j)[2-k]=image_V(idx,k);
        }
      }
    }
  }
}

void ColorTransfer::RunColorTransfer(){
  //std::string input_img1, input_img2;
  //std::cout<<"input 1.jpg or 2.jpg or 3.png or 4.png"<<std::endl;
  //std::cin>>input_img1;
  //std::cin>>input_img2;
  //LoadImage(input_img1,input_img2);
  LoadImage("1.jpg","2.jpg");
  InitMatrix();
  InitTransfer();
  RgbtoLms(image_souce_v_,height_souce_,width_souce_);
  RgbtoLms(image_target_v_,height_target_,width_target_);
  GetMean(mean_souce_,image_souce_v_,height_souce_,width_souce_);
  GetMean(mean_target_,image_target_v_,height_target_,width_target_);
  GetDelta(delta_souce_,mean_souce_,image_souce_v_,height_souce_,width_souce_);
  GetDelta(delta_target_,mean_target_,image_target_v_,height_target_,width_target_);
  Transfer(height_souce_,width_souce_);
  LabtoRgb(image_souce_v_,height_souce_,width_souce_);
  GetResultImage(image_result_,image_souce_v_);
  cv::imwrite("output.bmp",image_result_);
}


