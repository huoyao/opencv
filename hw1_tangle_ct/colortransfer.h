#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <cmath>
#include <Eigen/Core>
#include <Eigen/Dense>

namespace tl{
class ColorTransfer
{
public:
  ColorTransfer(void);
  ~ColorTransfer(void);
  bool LoadImage( const std::string imagename_souce, const std::string imagename_target);
  void InitMatrix();
  void InitTransfer();
  void GetPixelData( const cv::Mat &img, Eigen::MatrixXd &pixel_data);
  void RgbtoLms( Eigen::MatrixXd &img_v, const int& height, const int& width);
  void LabtoRgb(Eigen::MatrixXd &img_v, const int& height, const int& width);
  void GetMean(Eigen::Vector3d &mean, const Eigen::MatrixXd &img_v, const int& height, const int& width);
  void GetDelta(Eigen::Vector3d &delta, const Eigen::Vector3d &mean, const Eigen::MatrixXd &img_v, const int& height, const int& width);
  void Transfer( const int& height, const int& width);
  void GetResultImage(cv::Mat &img_rerult, const Eigen::MatrixXd &image_V );
  void RunColorTransfer();
  cv::Mat GetImgeSouce(){return image_souce_;}
  cv::Mat GetImgeTarget(){return image_target_;}
  cv::Mat GetImgeResult(){return image_result_;}
private:
  Eigen::MatrixXd image_souce_v_;
  Eigen::MatrixXd image_target_v_;	
  Eigen::Vector3d mean_souce_, mean_target_, delta_souce_, delta_target_;
  int height_souce_, width_souce_, height_target_, width_target_;
  cv::Mat image_souce_;
  cv::Mat image_target_;
  cv::Mat image_result_;
  Eigen::Matrix3d rgbtolms_;
  Eigen::Matrix3d lmstolab_;
  Eigen::Matrix3d labtolms_;
  Eigen::Matrix3d lmstorgb_;
};
}
