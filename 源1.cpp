#include <iostream>
#include <vector>
#include <string>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2nfree/features2d.hpp>
#include <opencv2/legacy/legacy.hpp>

using namespace std;
using namespace cv;
 
void num2img(Mat& image, int value);
int main()
{
	//读入输入图像
	Mat img1=imread("image1.jpg");
	Mat img2=imread("image2.jpg");
	if (!img1.data || !img2.data)
		return 0; 
/*
    //展示原始图像
	namedWindow("Image1");
	imshow("Image1",img1);
	namedWindow("Image2");
	imshow("Image2",img2);
*/
	//提取两图像的SIFT特征点
	vector<KeyPoint> keypoints1;
	vector<KeyPoint> keypoints2;

	//构造SIFT特征检测器
	SiftFeatureDetector sift(0,3,0.1);

	//检测两图的SIFT特征
	sift.detect(img1,keypoints1);
	sift.detect(img2,keypoints2);
	cout<<"Image1的特征点数目为："<<keypoints1.size()<<endl;
	cout<<"Image2的特征点数目为："<<keypoints2.size()<<endl;
/*
	//画出两图的特征点
	cv::Mat imageKP;
	cv::drawKeypoints(img1,keypoints1,imageKP,cv::Scalar::all(-1),cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS);
	cv::namedWindow("Right SURF Features");
	cv::imshow("Right SURF Features",imageKP);
	cv::drawKeypoints(img2,keypoints2,imageKP,cv::Scalar::all(-1),cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS);
	cv::namedWindow("Left SURF Features");
	cv::imshow("Left SURF Features",imageKP);
	*/

	//构造SIFT特征向量提取器
	SiftDescriptorExtractor siftDesc;

	//特征点向量的计算
	Mat descriptors1,descriptors2;
	siftDesc.compute(img1,keypoints1,descriptors1);
	siftDesc.compute(img2,keypoints2,descriptors2);

	//构造匹配起
	BruteForceMatcher<L2<float>> matcher;

	//匹配两幅图的描述子
	vector<DMatch> matches;
	matcher.match(descriptors1,descriptors2,matches);
	cout<<"总共匹配的特征点数目Match个数为："<<matches.size()<<endl;
/*
	//取前n个匹配最好的特征点
	nth_element(matches.begin(),
		matches.begin()+49,
		matches.end());
	matches.erase(matches.begin()+50,matches.end());
	*/

	/出匹配特征点之间的最短和最长欧氏距离，选出最好的Match点对
	double min_d=1000.0,max_d=0.0;
	for(int i=0;i<matches.size();++i)/出最大最小距离
	{
		if(matches[i].distance<min_d)
			min_d=matches[i].distance;
		if(matches[i].distance>max_d)
			max_d=matches[i].distance;
	}
	vector<DMatch> goodmatches;/出好的特征匹配点
	goodmatches.clear();
	for(int i=0;i<matches.size();++i)
	{
		if(matches[i].distance<0.3*max_d)
			goodmatches.push_back(matches[i]);
	}
	cout<<"总共匹配的特征点中好的匹配特征点个数为："<<goodmatches.size()<<endl;

	//画出匹配的特征点
	//Mat featureImage1,featureImage2;
	int n=goodmatches.size();
	//cout<<n<<endl;
	vector<KeyPoint> keymatches1;
	vector<KeyPoint> keymatches2;
	keymatches1.clear();
	keymatches2.clear();
	//给每个匹配的特征点存到对应的图像的vector中
	for(int i=0;i<n;++i)
	{
		keymatches1.push_back(keypoints1[goodmatches[i].queryIdx]);
		keymatches2.push_back(keypoints2[goodmatches[i].trainIdx]);
	}

	//画出每个图中的特征点
	int b,g,r;
	for(int i=0;i<n;++i)
	{
		b=rand()%256;
		g=rand()%256;
	    r=rand()%256;
		circle(img1,keymatches1[i].pt,keymatches1[i].size/2,Scalar(b,g,r),5);
		circle(img2,keymatches2[i].pt,keymatches2[i].size/2,Scalar(b,g,r),5);
	}
	//cout<<keymatches1.size()<<endl;
	//cout<<keymatches2.size()<<endl;
	//imshow("Features in Image1",img1);
	//imshow("Features in Image2",img2);
	
	//给图片加入匹配特征点的数目
	 num2img(img1,goodmatches.size());
	 num2img(img2,goodmatches.size());

	//显示这两幅带有特征点的图
	//Mat dst1,dst2;
	namedWindow("Features in Image1");
	resize(img1,img1,Size(0,0),0.4,0.4);
	//resizeWindow("Features in Image1",100,100);
	imshow("Features in Image1",img1);
	namedWindow("Features in Image2");
	resize(img2,img2,Size(0,0),0.4,0.4);
	//resizeWindow("Features in Image2",100,100);
	imshow("Features in Image2",img2);
	

	waitKey();
	return 0;
}

string num2string(int value)
{
	char buf[10];
	sprintf(buf,"%d",value);
	string str=buf;
	return str;
}

void num2img(Mat& image, int value)
{
	 
    Scalar textColor =cvScalar(0,0,0);
	putText(image,num2string(value),Point(image.cols-140,image.rows-30),FONT_HERSHEY_SIMPLEX,2,textColor,2);
}